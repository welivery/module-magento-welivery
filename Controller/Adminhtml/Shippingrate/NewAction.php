<?php
/**
 *
 * @name Ids\Welivery\Controller\Adminhtml\Shippingrate\NewAction
 *
 * @description Controller action to add postal codes shipping rate
 *
 */
namespace Ids\Welivery\Controller\Adminhtml\Shippingrate;
use Magento\Backend\App\Action;
class NewAction extends Action
{
    /**
     *
     * @var \Magento\Framework\View\Result\PageFactory
     *
     */
    protected $resultPageFactory;

    /**
     *
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     *
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     *
     * Postcodes shipping rate add form
     *
     * @return \Magento\Backend\Model\View\Result\Page
     *
     */
    public function execute()
    {
        $data = $this->getRequest()->getParam('shippingrate');

        /**
         *
         * If there is a 'save' action, there is a 'shippingrate' array param
         * Save and redirect to the grid
         *
         */
        if(is_array($data)) {
            /**
             *
             * Get Welivery shipping rate model
             *
             */
            $shippingRate = $this->_objectManager->create('Ids\Welivery\Model\Shippingrate');

            /**
             *
             * Save string of postal codes with the form "PC1,PC2,PC3.1-PC3.2,PC4"
             *
             */
            $shippingRate->savePostalCodesRate($data['postcodes'], $data['rate']);

            /**
             *
             * Redirect to grid page
             *
             */
            return $this->resultRedirectFactory->create()->setPath('*/*/index');
        }

        /**
         *
         * If there is a 'new' action, render the new form
         *
         */
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Ids_Welivery::postcodes_shippingrate');
        $resultPage->getConfig()->getTitle()->prepend(__('New Rate'));
        return $resultPage;
    }
}
