<?php
/**
 *
 * @name Ids\Welivery\Setup\UpgradeSchema
 *
 * @description Upgrade schema
 *
 */
namespace Ids\Welivery\Setup;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
class UpgradeSchema implements  UpgradeSchemaInterface
{
    /**
     *
     * Upgrade schema
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context){
        $setup->startSetup();

        /**
         *
         * If the version of the module is lower than '0.0.2', run this upgrade
         *
         */
        if (version_compare($context->getVersion(), '0.0.2') < 0) {
            /**
             *
             * Get table name
             *
             */
            $tableName = $setup->getTable('sales_shipment');

            /**
             *
             * Check if the table alredy exists
             *
             */
            if ($setup->getConnection()->isTableExists($tableName)) {
                $connection = $setup->getConnection();

                /**
                 *
                 * Add column to save welivery delivery created ID
                 *
                 */
                $connection->addColumn($tableName, 'welivery_id', array(
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size'     => 255,
                    'nullable' => false,
                    'comment'  => 'Welivery Delivery ID'
                ));
            }
        }

        /**
         *
         * If the version of the module is lower than '0.0.3', run this upgrade
         *
         */
        if (version_compare($context->getVersion(), '0.0.3') < 0) {
            /**
             *
             * Get table name
             *
             */
            $tableName = $setup->getTable('sales_shipment');

            /**
             *
             * Check if the table alredy exists
             *
             */
            if ($setup->getConnection()->isTableExists($tableName)) {
                $connection = $setup->getConnection();

                /**
                 *
                 * Rename the column 'welivery_id' to be 'custom_welivery_ids'.
                 * We have to make a difference between the 'Id' param needed to create the shipment in 'Welivery' and the 'IdWelivery' used to track the shipment
                 *
                 * @note It is going to have the 'Id' params concatenated by a colon. It is the best way to save this data because when i create the 'Welivery' shipment, i do not have a shipment ID to make a One To Many relationship table
                 *
                 */
                $connection->changeColumn($tableName, 'welivery_id', 'custom_welivery_ids', array(
                    'type'     => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'size'     => 255,
                    'nullable' => false,
                    'comment'  => 'Custom Welivery Delivery ID'
                ));
            }
        }

        $setup->endSetup();
    }
}
