<?php
/**
 *
 * @name Ids\Welivery\Controller\Adminhtml\Shippinglabel\Shipment\MassGenerateShippingLabel
 *
 * @description Mass generate shipping labels action
 *
 */
namespace Ids\Welivery\Controller\Adminhtml\Shippinglabel\Shipment;

/**
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 */
class MassGenerateShippingLabel extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
    /**
     *
     * @var \Magento\Shipping\Model\Shipping\LabelGenerator
     *
     */
    protected $labelGenerator;

    /**
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     *
     */
    protected $collectionFactory;

    /**
     *
     * @var \Magento\Framework\App\Response\Http\FileFactory
     *
     */
    protected $fileFactory;

    /**
     *
     * Constructor
     *
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory
     * @param \Magento\Shipping\Model\Shipping\LabelGenerator $labelGenerator
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     *
     */
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        \Magento\Shipping\Model\Shipping\LabelGenerator $labelGenerator,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Ui\Component\MassAction\Filter $filter
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->labelGenerator    = $labelGenerator;
        $this->fileFactory       = $fileFactory;
        parent::__construct($context, $filter);
    }

    /**
     *
     * Create shipment. Batch print shipping labels for whole shipments. Push pdf document with shipping labels to user browser
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $collection
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     *
     */
    protected function massAction(\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $collection)
    {
        $labelsContent = [];

        foreach($collection as $order) {
            $shipment = $order->createShipmentAndWelivery();

            /**
             *
             * Concatenate shipping labels
             *
             */
            $labelContent = $shipment->getShippingLabel();
            if ($labelContent) {
                $labelsContent[] = $labelContent;
            }
        }

        /**
         *
         * Return shipping labels
         *
         */
        if(!empty($labelsContent)) {
            $outputPdf = $this->labelGenerator->combineLabelsPdf($labelsContent);
            return $this->fileFactory->create(
                'ShippingLabels.pdf',
                $outputPdf->render(),
                \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,
                'application/pdf'
            );
        }

        /**
         *
         * If there are not shipping labels, there was an error
         *
         */
        $this->messageManager->addError(__('There are no shipping labels related to selected orders.'));
        return $this->resultRedirectFactory->create()->setPath('welivery/shippinglabel/');
    }
}
