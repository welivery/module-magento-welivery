<?php
/**
 *
 * @name  \Ids\Welivery\Observer\CancelShipment
 *
 * @description Cancel order observer
 *
 */
namespace Ids\Welivery\Observer;
class CancelShipment implements \Magento\Framework\Event\ObserverInterface
{
    /**
     *
     * Constructor
     *
     */
    public function __construct()
    {}

    /**
     *
     * Cancel order event catch
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return void
     *
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /**
         *
         * @var $order \Magento\Sales\Model\Order
         *
         */
        $order = $observer->getOrder();

        /**
         *
         * Validate if order shipping method is 'Welivery'
         *
         */
        if($order->isShippingMethodWelivery()) {
            /**
             *
             * Cancel Welivery shipments in Welivery platform
             *
             */
            foreach($order->getShipmentsCollection() as $shipment) {
                $shipment->cancelWelivery();
            }
        }
    }
}