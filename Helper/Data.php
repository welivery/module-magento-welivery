<?php
/**
 *
 * @name Ids\Welivery\Helper\Data
 *
 * @description Welivery Helper
 *
 */
namespace Ids\Welivery\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     *
     * Convert kilograms to grams
     *
     * @param int $kg Kilograms
     *
     * @return int
     *
     */
    public function convertKgToG($kg)
    {
        return $kg * 1000;
    }

    /**
     *
     * Convert pounds to grams
     *
     * @param int $pound Pounds
     *
     * @return int
     *
     */
    public function convertPoundToG($pound)
    {
        return $pound * 453.592;
    }

    /**
     *
     * Convert inches to centimeters
     *
     * @param int $inch Inches
     *
     * @return int
     *
     */
    public function convertInchToCm($inch)
    {
        return $inch * 2.54;
    }
}
