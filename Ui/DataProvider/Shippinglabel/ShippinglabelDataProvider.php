<?php
/**
 *
 * @name Ids\Welivery\Ui\DataProvider\Shippinglabel\ShippinglabelDataProvider
 *
 * @description Shipping label listing data provider
 *
 */
namespace Ids\Welivery\Ui\DataProvider\Shippinglabel;
class ShippinglabelDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     *
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collection Sales order collection
     *
     */
    protected $collection;

    /**
     *
     * @var \Magento\Ui\DataProvider\AddFieldToCollectionInterface[]
     *
     */
    protected $addFieldStrategies;

    /**
     *
     * @var \Magento\Ui\DataProvider\AddFilterToCollectionInterface[]
     *
     */
    protected $addFilterStrategies;

    /**
     *
     * Construct
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory
     * @param \Magento\Ui\DataProvider\AddFieldToCollectionInterface[] $addFieldStrategies
     * @param \Magento\Ui\DataProvider\AddFilterToCollectionInterface[] $addFilterStrategies
     * @param array $meta
     * @param array $data
     *
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        array $addFieldStrategies  = [],
        array $addFilterStrategies = [],
        array $meta                = [],
        array $data                = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection          = $collectionFactory->create();
        $this->addFieldStrategies  = $addFieldStrategies;
        $this->addFilterStrategies = $addFilterStrategies;
    }

    /**
     *
     * Get data
     *
     * @return array
     *
     */
    public function getData()
    {
        if(!$this->getCollection()->isLoaded()) {
            /**
             *
             * Filter order collection to get only 'new' orders and 'Welivery' as its shipping method
             *
             */
            $this->getCollection()->addFieldToFilter('state', array('eq' => \Magento\Sales\Model\Order::STATE_NEW))
                                  ->addFieldToFilter('shipping_method', array('eq' => 'welivery_welivery'))
                                  ->load();
        }

        $items = $this->getCollection()->toArray();

        return $items;
    }

    /**
     *
     * Add field to select
     *
     * @param string|array $field
     * @param string|null $alias
     *
     * @return void
     *
     */
    public function addField($field, $alias = null)
    {
        if(isset($this->addFieldStrategies[$field])) {
            $this->addFieldStrategies[$field]->addField($this->getCollection(), $field, $alias);
        }
        else {
            parent::addField($field, $alias);
        }
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        if(isset($this->addFilterStrategies[$filter->getField()])) {
            $this->addFilterStrategies[$filter->getField()]
                 ->addFilter(
                    $this->getCollection(),
                    $filter->getField(),
                    [$filter->getConditionType() => $filter->getValue()]
                 );
        }
        else {
            parent::addFilter($filter);
        }
    }
}
