<?php
/**
 *
 * @name Ids\Welivery\Model\Carrier\AbstractWelivery
 *
 * @description 'Welivery' shipping method abstract class
 *
 */
namespace Ids\Welivery\Model\Carrier;
abstract class AbstractWelivery extends \Magento\Shipping\Model\Carrier\AbstractCarrierOnline
{
    /**
     *
     * @var object 'Welivery' rate factory model to get rate associated with a postal code. Use table 'ids_welivery_shippingrate'
     *
     */
    protected $_weliveryRateFactory;

    /**
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory
     *
     */
    protected $_trackCollectionFactory;

    /**
     *
     * Constructor
     *
     * @param \Ids\Welivery\Model\ShippingrateFactory $weliveryRateFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory $trackCollectionFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Xml\Security $xmlSecurity
     * @param \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory
     * @param \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory
     * @param \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Directory\Helper\Data $directoryData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     *
     */
    public function __construct(
        \Ids\Welivery\Model\ShippingrateFactory $weliveryRateFactory,
        \Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory $trackCollectionFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Xml\Security $xmlSecurity,
        \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        array $data = []
    ) {
        /**
         *
         * Set welivery rate factory model. Get rate associated with a postal code.
         *
         */
        $this->_weliveryRateFactory = $weliveryRateFactory;

        /**
         *
         * Set track collection factory model
         *
         */
        $this->_trackCollectionFactory = $trackCollectionFactory;

        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $data
        );
    }

    /**
     *
     * Return rate price associated to an specific postal code
     *
     * @param string $postCode Postal code to get rate price
     *
     * @return number|NULL Return the rate or NULL in case that there is not a rate price to the postal code
     *
     */
    public function getRatePriceByPostCode($postCode)
    {
        return $this->_weliveryRateFactory->create()->getRateByPostCode($postCode);
    }
}
