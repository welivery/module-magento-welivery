<?php
/**
 *
 * @name Ids\Welivery\Ui\DataProvider\Shippingrate\Form\ShippingrateDataProvider
 *
 * @description Postal codes shipping rate data provider
 *
 */
namespace Ids\Welivery\Ui\DataProvider\Shippingrate\Form;
class ShippingrateDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     *
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Ids\Welivery\Model\ResourceModel\Shippingrate\CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     *
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Ids\Welivery\Model\ResourceModel\Shippingrate\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }
}