<?php
/**
 *
 * @name Ids\Welivery\Controller\Index\Shippingcalc
 *
 * @description Welivery shipping calculation action
 *
 */
namespace Ids\Welivery\Controller\Index;
class Shippingcalc extends \Magento\Catalog\Controller\Product\View
{
    /**
     *
     * @var \Magento\Framework\Controller\Result\JsonFactory Return JSON data to AJAX call
     *
     */
    protected $_resultJsonFactory;

    /**
     *
     * @var \Magento\Framework\Pricing\Helper\Data Price helper to format rate
     *
     */
    protected $_priceHelper;

    /**
     *
     * @var object 'Welivery' rate factory model to get rate associated with a postal code. Use table 'ids_welivery_shippingrate'
     *
     */
    protected $_weliveryRateFactory;

    /**
     *
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Catalog\Helper\Product\View $viewHelper
     * @param \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\Pricing\Helper\Data $priceHelper
     * @param \Ids\Welivery\Model\ShippingrateFactory $weliveryRateFactory
     *
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Catalog\Helper\Product\View $viewHelper,
        \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Ids\Welivery\Model\ShippingrateFactory $weliveryRateFactory
    ) {
        /**
         *
         * Set the json responder
         *
         */
        $this->_resultJsonFactory = $resultJsonFactory;

        /**
         *
         * Set the price helper
         *
         */
        $this->_priceHelper = $priceHelper;

        /**
         *
         * Set welivery rate factory model. Get rate associated with a postal code.
         *
         */
        $this->_weliveryRateFactory = $weliveryRateFactory;

        parent::__construct($context, $viewHelper, $resultForwardFactory, $resultPageFactory);
    }

    /**
     *
     * Shipping calculation action
     *
     * @return \Magento\Framework\Controller\Result\Forward|\Magento\Framework\Controller\Result\Redirect. Return the price rate or an error message in case there is not rate for the postal code
     *
     * @note Get POST data: 'postcode' -> Postal code to get the rate - 'ajax' -> If it is an AJAX call
     *
     */
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        $params = $this->getRequest()->getParams();

        if($this->getRequest()->isAjax()) {
            $shipRate = $this->_weliveryRateFactory->create();
            $rate     = $shipRate->getRateByPostCode($params['postcode']);

            /**
             *
             * Validate if rate is not equal to 0
             *
             */
            if($rate) {
                $result->setData(['response' => $this->_priceHelper->currency($rate, true, false)]);
            }
            else {
                $result->setData(['response' => __('There are no submissions for that zip code')]);
            }
        }

        return $result;
    }
}