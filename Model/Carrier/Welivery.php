<?php
/**
 *
 * @name Ids\Welivery\Model\Carrier\Welivery
 *
 * @description Class to manage the shipping method
 *
 */
namespace Ids\Welivery\Model\Carrier;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
class Welivery extends AbstractWelivery implements \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     *
     * @var string Shipping method code. Has to be the same as etc/adminhtml/system.xml -> <config> -> <system> -> <section id="carriers"> -> <group id="welivery">
     *
     */
    protected $_code = 'welivery';

    /**
     *
     * Return array with the shipping allowed methods
     *
     * @return array
     *
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }

    /**
     *
     * Processing additional validation to check if carrier applicable.
     *
     * @param \Magento\Framework\DataObject $request
     *
     * @return $this|bool|\Magento\Framework\DataObject
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     *
     */
    public function proccessAdditionalValidation(\Magento\Framework\DataObject $request)
    {
        $configErrorMsg = $this->getConfigData('specificerrmsg');
        $showMethod     = $this->getConfigData('showmethod');
        $ratePrice      = $this->getRatePriceByPostCode($request->getDestPostcode());

        /**
         *
         * If there is not rate price to the postal code given, there is an error
         *
         */
        if(!$ratePrice) {
            if($showMethod) {
                $error = $this->_rateErrorFactory->create();
                $error->setCarrier($this->_code);
                $error->setCarrierTitle($this->getConfigData('title'));
                $error->setErrorMessage($configErrorMsg);

                return $error;
            }

            return false;
        }

        return $this;
    }

    /**
     *
     * Method to create shipping method with its shipping amount
     *
     * @param RateRequest $request Request data to create shipping method
     *
     * @return bool|Result
     *
     */
    public function collectRates(RateRequest $request)
    {
        if(!$this->canCollectRates()) {
            return $this->getErrorMessage();
        }

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateFactory->create();

        /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
        $method = $this->_rateMethodFactory->create();

        /**
         *
         * Create the shipping carrier
         *
         */
        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));

        /**
         *
         * Create the shipping method
         *
         */
        $method->setMethod($this->_code);
        $method->setMethodTitle($this->getConfigData('name'));

        /**
         *
         * Get rate by postal code
         *
         */
        $amount = $this->getRatePriceByPostCode($request->getDestPostcode());

        $method->setPrice($amount);
        $method->setCost($amount);

        $result->append($method);

        return $result;
    }

    /**
     *
     * Check if carrier has shipping tracking option available
     *
     * @return bool
     *
     * @note The tracking of this shipping method is going to be do outside of the platform
     *
     */
    public function isTrackingAvailable()
    {
        return false;
    }

    /**
     *
     * Get tracking
     *
     * @param string|string[] $trackings
     *
     * @return \Magento\Shipping\Model\Tracking\Result
     *
     */
    public function getTracking($trackings)
    {
        if (!is_array($trackings)) {
            $trackings = [$trackings];
        }

        $result = $this->_trackFactory->create();

        foreach ($trackings as $tracking) {
            $status          = $this->_trackStatusFactory->create();
            $trackCollection = $this->_trackCollectionFactory->create();

            /**
             *
             * Get track data
             *
             */
            $tracks = $trackCollection->addFieldToFilter('track_number', array('eq' => $tracking));
            $track  = $tracks->getFirstItem();

            /**
             *
             * Get shipment from track
             *
             */
            $shipment = $track->getShipment();

            /**
             *
             * Get 'Welivery' data from shipment tracking number
             *
             * @note it is the best way to get the 'Welivery' data. It's hard to work with 'custom_welivery_ids' (data to match with 'Welivery' API)
             *
             */
            $weliveryData = $shipment->getWeliveryDataByTrackingNumber($tracking);

            $status->setCarrier($this->_code);
            $status->setCarrierTitle($this->getConfigData('title'));
            $status->setTracking($tracking);
            $status->setPopup(1);
            $status->setUrl($weliveryData['TrackingUrl']);
            $result->append($status);
        }

        return $result;
    }

    /**
     *
     * Check if carrier has shipping label option available
     *
     * @return bool
     *
     * @note This shipping method allow create shipping labels without extra configuration
     *
     */
    public function isShippingLabelsAvailable()
    {
        return true;
    }

    /**
     *
     * Do shipment request to carrier web service, obtain Print Shipping Labels and process errors in response
     *
     * @param \Magento\Framework\DataObject $request
     *
     * @return \Magento\Framework\DataObject
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     */
    protected function _doShipmentRequest(\Magento\Framework\DataObject $request)
    {
        $result = new \Magento\Framework\DataObject();

        try {
            /**
             *
             * Create 'Welivery' shipment
             *
             */
            $data = $request->getOrderShipment()->createWelivery($request->getPackageParams()->getData());

            $result->setTrackingNumber($data['IdWelivery']);
            $result->setShippingLabelContent($data['shipping_label']);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        }

        return $result;
    }
}