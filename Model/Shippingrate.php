<?php
/**
 *
 * @name Ids\Welivery\Model\Shippingrate
 *
 * @description Model to work with 'ids_welivery_shippingrate' table
 *
 * @todo See to use a Repository instead of this CRUD model structure with API Interface
 *
 */
namespace Ids\Welivery\Model;
class Shippingrate extends \Magento\Framework\Model\AbstractModel
{
    /**
     *
     * @const POSTAL_CODES_DELIMITER Character delimiter in postal codes input
     *
     */
    const POSTAL_CODES_DELIMITER = ',';

    /**
     *
     * @const RANGE_DELEMITER Character delimiter in range input
     *
     */
    const RANGE_DELEMITER = '-';

    /**
     *
     * Initialize model
     *
     * @return void
     *
     */
    protected function _construct()
    {
        $this->_init('Ids\Welivery\Model\ResourceModel\Shippingrate');
    }

    /**
     *
     * Get rate by a postal code given
     *
     * @param string $postCode Postal code
     *
     * @return number | NULL Rate from the postal code given
     *
     * @note If there is overlaps of ranges, the last item inserted has more priority
     *
     */
    public function getRateByPostCode($postCode)
    {
        $collection = $this->getCollection();

        $collection->addFieldToFilter('postcode_from', array('lteq' => $postCode))
                   ->addFieldToFilter('postcode_to', array('gteq'  => $postCode));

        return $collection->getLastItem()->getRate();
    }

    /**
     *
     * Save rate to multiple postal codes
     *
     * @param string $postalCodes Postal codes with the form "PC1,PC2,PC3.1-PC3.2,PC4"
     * @param number|string $rate Rate to the postal codes
     *
     * @note This save method does not validate if there are overlaps in the postal code ranges. If there are overlaps, allways the last inserted has higher priority
     *
     */
    public function savePostalCodesRate($postalCodes, $rate)
    {
        /**
         *
         * Get postal codes
         *
         */
        $postCodes = $this->_getPostalCodesFromString($postalCodes);

        /**
         *
         * Loop postal codes
         *
         */
        foreach($postCodes as $postCode) {
            /**
             *
             * Validate there is not an empty string
             *
             */
            if(!empty(trim($postCode))) {
                /**
                 *
                 * Set by default 'from' and 'to' range. If it is a range, overwrite them
                 *
                 */
                $from = $postCode;
                $to   = $postCode;

                /**
                 *
                 * If it is a range
                 *
                 */
                if($this->_isPostalCodeRange($postCode)) {
                    list($from, $to) = $this->_getPostalCodesFromRange($postCode);

                    /**
                     *
                     * If in the range given, the 'from' is higher than the 'to' -> swap
                     *
                     */
                    if($from > $to) {
                        list($from, $to) = array($to, $from);
                    }
                }

                $this->setPostcodeFrom(trim($from));
                $this->setPostcodeTo(trim($to));
                $this->setRate($rate);
                $this->save();

                /**
                 *
                 * @note Necessary to unset data to do not overwrite the same row
                 *
                 */
                $this->unsetData();
            }
        }
    }

    /**
     *
     * Return an array of postal codes
     *
     * @param string $postalCodesStr Postal codes string with the form "PC1,PC2,PC3.1-PC3.2,PC4"
     *
     * @return array Postal codes array with the form [0 => "PC1", 1 => "PC2.1-PC2-2", 2 => "PC3"]
     *
     */
    private function _getPostalCodesFromString($postalCodesStr)
    {
        return explode(self::POSTAL_CODES_DELIMITER, $postalCodesStr);
    }

    /**
     *
     * Evaluate if it is a range of postal codes or not
     *
     * @param $postCode String with the form "PC1" OR "PC1.1-PC1.2"
     *
     * @return bool|int True if it is a range - False if it is not a range
     *
     */
    private function _isPostalCodeRange($postCode)
    {
        return strpos($postCode, self::RANGE_DELEMITER);
    }

    /**
     *
     * Return an array woth the range of postal codes
     *
     * @param string $range String with the form "PC1.1-PC1.2"
     *
     * @return array Array with the form [0 => "PC_FROM", 1 => "PC_TO"]
     *
     */
    private function _getPostalCodesFromRange($range)
    {
        return explode(self::RANGE_DELEMITER, $range);
    }
}