<?php
/**
 *
 * @name Ids\Welivery\Model\Sales\Order
 *
 * @description Override order model
 *
 */
namespace Ids\Welivery\Model\Sales;
class Order extends \Magento\Sales\Model\Order
{
    /**
     *
     * @var \Magento\Sales\Model\Convert\OrderFactory
     *
     */
    protected $_convertOrderFactory;

    /**
     *
     * @var \Magento\Shipping\Model\ShipmentNotifierFactory
     *
     */
    protected $_shipmentNotifierFactory;

    /**
     *
     * Constructor class
     *
     * @param \Magento\Sales\Model\Convert\OrderFactory $convertOrderFactory
     * @param \Magento\Shipping\Model\ShipmentNotifierFactory $shipmentNotifierFactory,
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Sales\Model\Order\Config $orderConfig
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $orderItemCollectionFactory
     * @param \Magento\Catalog\Model\Product\Visibility $productVisibility
     * @param \Magento\Sales\Api\InvoiceManagementInterface $invoiceManagement
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Sales\Model\Order\Status\HistoryFactory $orderHistoryFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\Address\CollectionFactory $addressCollectionFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\Payment\CollectionFactory $paymentCollectionFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory $historyCollectionFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollectionFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $shipmentCollectionFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory $memoCollectionFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory $trackCollectionFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $salesOrderCollectionFactory
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productListFactory
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     *
     */
    public function __construct(
        \Magento\Sales\Model\Convert\OrderFactory $convertOrderFactory,
        \Magento\Shipping\Model\ShipmentNotifierFactory $shipmentNotifierFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Sales\Model\Order\Config $orderConfig,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $orderItemCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\Sales\Api\InvoiceManagementInterface $invoiceManagement,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Sales\Model\Order\Status\HistoryFactory $orderHistoryFactory,
        \Magento\Sales\Model\ResourceModel\Order\Address\CollectionFactory $addressCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Payment\CollectionFactory $paymentCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory $historyCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory $shipmentCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory $memoCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory $trackCollectionFactory,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $salesOrderCollectionFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productListFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        /**
         *
         * Set convert order model to use when create shipment
         *
         */
        $this->_convertOrderFactory = $convertOrderFactory;

        /**
         *
         * Set shipment notifier model to use when create shipment
         *
         */
        $this->_shipmentNotifierFactory = $shipmentNotifierFactory;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $timezone,
            $storeManager,
            $orderConfig,
            $productRepository,
            $orderItemCollectionFactory,
            $productVisibility,
            $invoiceManagement,
            $currencyFactory,
            $eavConfig,
            $orderHistoryFactory,
            $addressCollectionFactory,
            $paymentCollectionFactory,
            $historyCollectionFactory,
            $invoiceCollectionFactory,
            $shipmentCollectionFactory,
            $memoCollectionFactory,
            $trackCollectionFactory,
            $salesOrderCollectionFactory,
            $priceCurrency,
            $productListFactory,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     *
     * Validate if order shipping method is 'Welivery'
     *
     * @return bool
     *
     */
    public function isShippingMethodWelivery()
    {
        return $this->getShippingMethod() == 'welivery_welivery';
    }

    /**
     *
     * Create store shipment and 'Welivery' shipment and notify customer
     *
     * @return \Magento\Sales\Model\Order\Shipment Created shipment
     *
     * @throws \Magento\Framework\Exception\LocalizedException Error creating order shipment and 'Welivery' shipment
     *
     */
    public function createShipmentAndWelivery()
    {
        try {
            /**
             *
             * Create store shipment
             *
             */
            $shipment = $this->_createShipment();

            /**
             *
             * Create packages and  'Welivery' shipment
             *
             */
            $shipment->createPackagesAndWelivery();

            /**
             *
             * Send shipment email
             *
             */
            $notifier = $this->_shipmentNotifierFactory->create();
            $notifier->notify($shipment);

            return $shipment;
        }
        catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        }
    }

    /**
     *
     * Create shipment from order
     *
     * @return \Magento\Sales\Model\Order\Shipment
     *
     * @throws \Magento\Framework\Exception\LocalizedException Error creating shipment
     *
     */
    protected function _createShipment()
    {
        /**
         *
         * Validate if can ship this order
         *
         */
        if(!$this->canShip()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('You can\'t create a shipment.'));
        }

        $convertOrder = $this->_convertOrderFactory->create();
        $shipment     = $convertOrder->toShipment($this);

        /**
         *
         * Add items to shipment
         *
         */
        foreach ($this->getAllItems() as $item) {
            $this->_addItemToShipment($item, $shipment);
        }

        /**
         *
         * Init Tracks
         *
         */
        $shipment->getTracksCollection();

        /**
         *
         * Register shipment
         *
         */
        $shipment->register();

        try {
            /**
             *
             * Set order in "Processing"
             *
             */
            $this->setIsInProcess(true);

            $shipment->save();
            $this->save();

            return $shipment;
        }
        catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        }
    }

    /**
     *
     * Add item to shipment
     *
     * @param \Magento\Sales\Model\Order\Item $item Item to add to shipment
     * @param \Magento\Sales\Model\Order\Shipment $shipment Shipment
     *
     */
    protected function _addItemToShipment(\Magento\Sales\Model\Order\Item $item, \Magento\Sales\Model\Order\Shipment $shipment)
    {
        $convertOrder = $this->_convertOrderFactory->create();

        /**
         *
         * Vaidate if can ship this item
         *
         */
        if (!$item->getQtyToShip() || $item->getIsVirtual()) {
            return;
        }

        $qtyShipped   = $item->getQtyToShip();
        $shipmentItem = $convertOrder->itemToShipmentItem($item);

        $shipmentItem->setQty($qtyShipped);
        $shipment->addItem($shipmentItem);
    }
}