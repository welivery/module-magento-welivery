<?php
/**
 *
 * @name Ids\Welivery\Block\Adminhtml\Shippingrate\Edit\Button\Generic
 *
 * @description Generic button class to postal codes shipping rate form
 *
 */
namespace Ids\Welivery\Block\Adminhtml\Shippingrate\Edit\Button;
class Generic
{
    /**
     *
     * Url Builder
     *
     * @var \Magento\Framework\UrlInterface
     *
     */
    protected $urlBuilder;

    /**
     *
     * Constructor
     *
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     *
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry
    ) {
        $this->urlBuilder = $context->getUrlBuilder();
    }

    /**
     *
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     *
     * @return  string
     *
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }
}
