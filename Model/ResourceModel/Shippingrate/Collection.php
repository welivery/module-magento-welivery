<?php
/**
 *
 * @name Ids\Welivery\Model\ResourceModel\Shippingrate\Collection
 *
 * @description Collection model to work with 'ids_welivery_shippingrate' table
 *
 * @todo See to use a Repository instead of this CRUD model structure with API Interface
 *
 */
namespace Ids\Welivery\Model\ResourceModel\Shippingrate;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     *
     * @var string
     *
     * @note We need this property to the mass delete action in a grid
     *
     * @todo See if we need this property or we can use a proxy
     *
     */
    protected $_idFieldName = 'entity_id';


    /**
     *
     * Initialize collection model
     *
     * @return void
     *
     */
    protected function _construct()
    {
        $this->_init('Ids\Welivery\Model\Shippingrate', 'Ids\Welivery\Model\ResourceModel\Shippingrate');
    }
}