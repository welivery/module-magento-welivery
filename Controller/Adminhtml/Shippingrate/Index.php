<?php
/**
 *
 * @name Ids\Welivery\Controller\Adminhtml\Shippingrate\Index
 *
 * @description Controller action to shipping rate management
 *
 */
namespace Ids\Welivery\Controller\Adminhtml\Shippingrate;
class Index extends \Magento\Backend\App\Action
{
    /**
     *
     * @var \Magento\Framework\View\Result\PageFactory
     *
     */
    protected $resultPageFactory;

    /**
     *
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     *
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     *
     * Postal codes shipping rate grid
     *
     * @return \Magento\Backend\Model\View\Result\Page
     *
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Ids_Welivery::postcodes_shippingrate');
        $resultPage->getConfig()->getTitle()->prepend(__('Shipping Rates'));
        return $resultPage;
    }
}
