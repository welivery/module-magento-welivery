<?php
/**
 *
 * @name registration.php
 *
 * @description Welivery Module registration
 *
 *
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Ids_Welivery',
    __DIR__
);