<?php
/**
 *
 * @name Ids\Welivery\Model\Config\Source\Shippinglabelsmode
 *
 * @description Set the shipping methods shipping label mode config value selection
 *
 */
namespace Ids\Welivery\Model\Config\Source;
class Shippinglabelsmode implements \Magento\Framework\Option\ArrayInterface
{
    /**
     *
     * @const SALE_MODE
     *
     * One shipping label per sale
     *
     */
    const SALE_MODE = 1;

    /**
     *
     * @const ITEM_MODE
     *
     * One shipping label per item
     *
     */
    const ITEM_MODE  = 2;

    /**
     *
     * Options getter
     *
     * @return array
     *
     */
    public function toOptionArray()
    {
        return [['value' => self::SALE_MODE, 'label' => __('One shipping label per sale')], ['value' => self::ITEM_MODE, 'label' => __('One shipping label per item')]];
    }

    /**
     *
     * Get options in "key-value" format
     *
     * @return array
     *
     */
    public function toArray()
    {
        return [self::SALE_MODE => __('One shipping label per sale'), self::ITEM_MODE => __('One shipping label per item')];
    }
}
