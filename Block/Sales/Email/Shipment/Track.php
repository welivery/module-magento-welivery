<?php
/**
 *
 * @name Ids\Welivery\Block\Sales\Email\Shipment
 *
 * @description Block to custom 'Welivery' shipment
 *
 */
namespace Ids\Welivery\Block\Sales\Email\Shipment;
use \Magento\Framework\View\Element\Template;
class Track extends Template
{
    /**
     *
     * @var \Magento\Shipping\Model\Order\TrackFactory
     *
     */
    protected $_trackFactory;

    /**
     *
     * Constructor
     *
     * @param \Magento\Shipping\Model\Order\TrackFactory $trackFactory
     * @param Template\Context $context
     * @param array $data
     *
     */
    public function __construct(
        \Magento\Shipping\Model\Order\TrackFactory $trackFactory,
        Template\Context $context,
        array $data = [])
    {
        $this->_trackFactory = $trackFactory;
        parent::__construct($context, $data);
    }

    /**
     *
     * Retrieve tracking by tracking entity id
     *
     * @return array
     *
     */
    public function getTrackingInfoByTrackId($trackId)
    {
        /** @var \Magento\Shipping\Model\Order\Track $track */
        $track = $this->_trackFactory->create()->load($trackId);
        if ($track->getEntityId()) {
            $result = $track->getNumberDetail();
        }
        else {
            $result = null;
        }

        return $result;
    }
}