<?php
/**
 *
 * @name Ids\Welivery\Setup\InstallSchema
 *
 * @description Create the table need it to manage the postal codes shipping rate
 *
 */
namespace Ids\Welivery\Setup;
class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     *
     * Install Tables
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     *
     */
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        if(!$installer->tableExists('ids_welivery_shippingrate')) {
            /**
             *
             * @note Create 'Welivery' table to manage postal codes shipping rate. Description fields:
             *
             *       - 'entity_id'    : Key row
             *       - 'rate'         : Rate to the postal code range. A little comment, it is not the best way but it could be negative to apply a discount
             *       - 'postcode_from': From where the range of postal codes begin to apply the rate. If it is the same as 'postcode_to', it's for only one postal code
             *       - 'postcode_to'  : To where the range of postal codes end up to apply the rate. If it is the same as 'postcode_from', it's for only one postal code
             *
             */
            $table = $installer->getConnection()->newTable($installer->getTable('ids_welivery_shippingrate'))
                                                ->addColumn(
                                                    'entity_id',
                                                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                                                    null,
                                                    [
                                                        'identity' => true,
                                                        'nullable' => false,
                                                        'primary'  => true,
                                                        'unsigned' => true,
                                                    ],
                                                    'Entity ID'
                                                )
                                                ->addColumn(
                                                    'rate',
                                                    \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                                                    '12,4',
                                                    [
                                                        'nullable' => false,
                                                        'default'  => '0.0000'
                                                    ],
                                                    'Rate')
                                                ->addColumn(
                                                    'postcode_from',
                                                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                                                    255,
                                                    [
                                                        'nullable' => false
                                                    ],
                                                    'Postal Code From'
                                                )
                                                ->addColumn(
                                                    'postcode_to',
                                                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                                                    255,
                                                    [
                                                        'nullable' => false
                                                    ],
                                                    'Postal Code To'
                                                )
                                                ->setComment('Postal Codes Shipping Rate');

            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
