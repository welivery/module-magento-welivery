<?php
/**
 *
 * @name Ids\Welivery\Controller\Adminhtml\Shippingrate\MassDelete
 *
 * @description Delete multiple shipping rates action
 *
 */
namespace Ids\Welivery\Controller\Adminhtml\Shippingrate;
class MassDelete extends \Magento\Backend\App\Action
{
    /**
     *
     * @var \Magento\Framework\View\Result\PageFactory
     *
     */
    protected $resultPageFactory;

    /**
     *
     * Mass actions filter
     *
     * @var \Magento\Ui\Component\MassAction\Filter
     *
     */
    protected $filter;

    /**
     *
     * @var \Ids\Welivery\Model\ResourceModel\Shippingrate\CollectionFactory
     *
     */
    protected $collectionFactory;

    /**
     *
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Ids\Welivery\Model\ResourceModel\Shippingrate\CollectionFactory $collectionFactory
     *
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Ui\Component\MassAction\Filter $filter,
        \Ids\Welivery\Model\ResourceModel\Shippingrate\CollectionFactory $collectionFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->filter            = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     *
     * Mass delete action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     *
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());

        $rateDeleted = 0;
        foreach ($collection->getItems() as $shippingRate) {
            $shippingRate->delete();
            $rateDeleted++;
        }

        $this->messageManager->addSuccess(
            __('A total of %1 record(s) have been deleted.', $rateDeleted)
        );

        return $this->resultRedirectFactory->create()->setPath('*/*/index');
    }
}
