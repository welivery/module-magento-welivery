<?php
/**
 *
 * @name Ids\Welivery\Block\Adminhtml\Shippingrate\Edit\Button\Save
 *
 * @description Postal codes shipping rate save form button
 *
 */
namespace Ids\Welivery\Block\Adminhtml\Shippingrate\Edit\Button;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
class Save extends Generic implements ButtonProviderInterface
{
    /**
     *
     * Return button data
     *
     * @return array
     *
     */
    public function getButtonData()
    {
        return [
            'label'          => __('Save'),
            'class'          => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 30
        ];
    }
}
