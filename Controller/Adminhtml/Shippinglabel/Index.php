<?php
/**
 *
 * @name Ids\Welivery\Controller\Adminhtml\Shippinglabel\Index
 *
 * @description Controller action to shipping labels management
 *
 */
namespace Ids\Welivery\Controller\Adminhtml\Shippinglabel;
class Index extends \Magento\Backend\App\Action
{
    /**
     *
     * @var \Magento\Framework\View\Result\PageFactory
     *
     */
    protected $resultPageFactory;

    /**
     *
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     *
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     *
     * Shipping Labels list page
     *
     * @return \Magento\Backend\Model\View\Result\Page
     *
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Ids_Welivery::shipping_labels');
        $resultPage->getConfig()->getTitle()->prepend(__('Shipping Labels'));
        return $resultPage;
    }
}
