<?php
/**
 *
 * @name Ids\Welivery\Model\Config\Source\Mode
 *
 * @description Set the shipping methods mode config value selection
 *
 */
namespace Ids\Welivery\Model\Config\Source;
class Mode implements \Magento\Framework\Option\ArrayInterface
{
    /**
     *
     * @const PROD_MODE
     *
     * Production mode value
     *
     */
    const PROD_MODE = 1;

    /**
     *
     * @const DEV_MODE
     *
     * Developer mode value
     *
     */
    const DEV_MODE  = 2;

    /**
     *
     * Options getter
     *
     * @return array
     *
     */
    public function toOptionArray()
    {
        return [['value' => self::PROD_MODE, 'label' => __('Production')], ['value' => self::DEV_MODE, 'label' => __('Developer')]];
    }

    /**
     *
     * Get options in "key-value" format
     *
     * @return array
     *
     */
    public function toArray()
    {
        return [self::PROD_MODE => __('Production'), self::DEV_MODE => __('Developer')];
    }
}
