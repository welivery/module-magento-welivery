<?php
/**
 *
 * @name  \Ids\Welivery\Observer\CreateShipment
 *
 * @description Place order after observer
 *
 */
namespace Ids\Welivery\Observer;
class CreateShipment implements \Magento\Framework\Event\ObserverInterface
{
    /**
     *
     * @var \Magento\Sales\Model\OrderFactory $_orderFactory Factory order model
     *
     */
    protected $_orderFactory;

    /**
     *
     * @var object 'Welivery' model factory object
     *
     */
    protected $_weliveryFactory;

    /**
     *
     * Constructor
     *
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Ids\Welivery\Model\WeliveryFactory $weliveryFactory
     *
     */
    public function __construct(
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Ids\Welivery\Model\WeliveryFactory $weliveryFactory
    ) {
        /**
         *
         * Set order factory  to create orders
         *
         */
        $this->_orderFactory = $orderFactory;

        /**
         *
         * Set 'Welivery' model factory object to interact with its API
         *
         */
        $this->_weliveryFactory = $weliveryFactory;
    }

    /**
     *
     * Place order after event catch
     *
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return void
     *
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $welivery                           = $this->_weliveryFactory->create();
        $orderIds                           = $observer->getOrderIds();
        $isShippingLabelGenerationAutomatic = $welivery->isShippingLabelGenerationAutomatic();

        foreach($orderIds as $id) {
            $order = $this->_orderFactory->create();
            $order->load($id);

            /**
             *
             * Validate if order shipping method is 'Welivery' and the shipping label generation flag is active
             *
             */
            if($order->isShippingMethodWelivery() && $isShippingLabelGenerationAutomatic) {
                $order->createShipmentAndWelivery();
            }
        }
    }
}