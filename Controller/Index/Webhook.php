<?php
/**
 *
 * @name Ids\Welivery\Controller\Index\Webhook
 *
 * @description Welivery webhook action. Update the order history
 *
 */
namespace Ids\Welivery\Controller\Index;
class Webhook extends \Magento\Framework\App\Action\Action
{
    /**
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory
     *
     */
    protected $_trackCollectionFactory;

    /**
     *
     * @var \Magento\Sales\Model\Order\Email\Sender\OrderCommentSender
     *
     */
    protected $_orderCommentSender;

    /**
     *
     * @var \Magento\Framework\Json\Helper\Data
     *
     */
    protected $_jsonHelper;

    /**
     *
     * @var \Psr\Log\LoggerInterface
     *
     */
    protected $_logger;

    /**
     *
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory $trackCollectionFactory
     * @param \Magento\Sales\Model\Order\Email\Sender\OrderCommentSender $orderCommentSender
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Psr\Log\LoggerInterface $logger
     *
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory $trackCollectionFactory,
        \Magento\Sales\Model\Order\Email\Sender\OrderCommentSender $orderCommentSender,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger
    ) {
        /**
         *
         * Set track collection factory model
         *
         */
        $this->_trackCollectionFactory = $trackCollectionFactory;

        /**
         *
         * Set order sender model
         *
         */
        $this->_orderCommentSender = $orderCommentSender;

        /**
         *
         * Set json helper
         *
         */
        $this->_jsonHelper = $jsonHelper;

        /**
         *
         * Set logger
         *
         */
        $this->_logger = $logger;

        parent::__construct($context);
    }

    /**
     *
     * 'Welivery' webhook action. Updated order status history
     *
     * @return void
     *
     * @throws \Magento\Framework\Exception\ValidatorException Error updating the order or sending the email
     *
     * @note Get POST data from body. See Welivery API 4.0 Documentation
     *
     */
    public function execute()
    {
        try {
            /**
             *
             * Debug Body data
             *
             */
            $this->_debug('Welivery Webhook: ');
            $this->_debug('Body-> ');
            $this->_debug($this->getRequest()->getContent());

            /**
             *
             * Get data from request BODY and parse it to work it like an array
             *
             */
            $data = array();
            parse_str($this->getRequest()->getContent(), $data);

            /**
             *
             * Debug Params data
             *
             */
            $this->_debug('Welivery Webhook: ');
            $this->_debug('Data-> ');
            $this->_debug($data);

            /**
             *
             * If it is not set the needed data, redirect to Home page
             *
             */
            if(!isset($data['data'])) {
                $this->_redirect('/');
                return;
            }

            $weliveryData = $data['data'];

            /**
             *
             * Debug 'Welivery' data
             *
             */
            $this->_debug('Welivery Data-> ');
            $this->_debug($weliveryData);

            /**
             *
             * Get track data
             *
             */
            $trackCollection = $this->_trackCollectionFactory->create();
            $tracks          = $trackCollection->addFieldToFilter('track_number', array('eq' => $weliveryData['IdWelivery']));
            $track           = $tracks->getFirstItem();

            /**
             *
             * Get shipment from track
             *
             */
            $shipment = $track->getShipment();

            /**
             *
             * Get order from shipment
             *
             */
            $order = $shipment->getOrder();

            /**
             *
             * Set message to notify
             *
             */
            $message = sprintf(__('Its shipment gets the status: %s'), $weliveryData['Status']);

            /**
             *
             * Notify customer
             *
             */
            $this->_orderCommentSender->send($order,  true, $message);

            /**
             *
             * Set status history
             *
             */
            $order->addStatusToHistory($order->getStatus(), $message, true);

            /**
             *
             * Save order
             *
             */
            $order->save();
        }
        catch(\Exception $e) {
            throw new \Magento\Framework\Exception\ValidatorException(__($e->getMessage()));
        }
    }

    /**
     *
     * Log debug data to file
     *
     * @param mixed $debugData
     *
     * @return void
     *
     */
    protected function _debug($debugData)
    {
        $this->_logger->debug(var_export($debugData, true));
    }
}