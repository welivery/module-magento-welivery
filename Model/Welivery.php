<?php
/**
 *
 * @name Ids\Welivery\Model\Welivery
 *
 * @description 'Welivery' Magento Model to interact with 'Welivery' API
 *
 */
namespace Ids\Welivery\Model;
class Welivery
{
    /**
     *
     * @const STANDARD_GROUP_ID API system config group to standard data
     *
     */
    const STANDARD_GROUP_ID = 'welivery_standard';

    /**
     *
     * @const PROD_GROUP_ID 'Welivery' API system config group to production data
     *
     */
    const PROD_GROUP_ID = 'welivery_prod';

    /**
     *
     * @const DEV_GROUP_ID 'Welivery' API system config group to developer data
     *
     */
    const DEV_GROUP_ID = 'welivery_dev';

    /**
     *
     * @const STATUS_ERROR_CODE 'Welivery' status error code given in an API call
     *
     */
    const STATUS_ERROR_CODE = 'Error';

    /**
     *
     * @var \Ids\Welivery\Helper\Data 'Welivery' helper
     *
     */
    protected $_helper;

    /**
     *
     * @var \Ids\Welivery\Factory\ApiFactory Custom 'Welivery' API factory object
     *
     */
    protected $_apiFactory;

    /**
     *
     * @var \Magento\Framework\Translate\Inline\StateInterface
     *
     */
    protected $_inlineTranslation;

    /**
     *
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     *
     */
    protected $_transportBuilder;

    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface Magento config
     *
     */
    protected $_scopeConfig;

    /**
     *
     * @var \Psr\Log\LoggerInterface
     *
     */
    protected $_logger;

    /**
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     *
     */
    protected $_date;

    /**
     *
     * @var array Map API 'Welivery' data (key) with request data given when ship is created (value)
     *
     */
    private $_mapShippingData = [
        'Address'    => 'street',
        'City'       => 'city',
        'PostalCode' => 'postcode',
        'Phone'      => 'telephone',
        'Email'      => 'email'
    ];

    /**
     *
     * @var array Map API 'Welivery' data (key) with the store data (value)
     *
     */
    private $_mapOriginData = [
        'OriginAddress'    => \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_ADDRESS1,
        'OirginCity'       => \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_CITY,
        'OriginPostalCode' => \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_ZIP
    ];

    /**
     *
     * Constructor
     *
     * @param \Ids\Welivery\Helper\Data $helper
     * @param \Ids\Welivery\Factory\ApiFactory $apiFactory
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     *
     */
    public function __construct(
        \Ids\Welivery\Helper\Data $helper,
        \Ids\Welivery\Factory\ApiFactory $apiFactory,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Stdlib\DateTime\DateTime $date
    ) {
        /**
         *
         * Set helper
         *
         */
        $this->_helper = $helper;

        /**
         *
         * Set API factory object to interact with 'Welivery'
         *
         */
        $this->_apiFactory = $apiFactory;

        /**
         *
         * Set inline translator to email
         *
         */
        $this->_inlineTranslation = $inlineTranslation;

        /**
         *
         * Set transport to send email
         *
         */
        $this->_transportBuilder = $transportBuilder;

        /**
         *
         * Set Magento config object
         *
         */
        $this->_scopeConfig = $scopeConfig;

        /**
         *
         * Set logger
         *
         */
        $this->_logger = $logger;

        /**
         *
         * Get date
         *
         */
        $this->_date = $date;
    }

    /**
     *
     * Create 'Welivery' shipment
     *
     * @param \Magento\Sales\Model\Order\Address $shippingAddress
     * @param array $package Package to ship. Array example:
     *        array (
     *          'weight'          => '1',
     *          'length'          => '1', (optional)
     *          'width'           => '1', (optional)
     *          'height'          => '1', (optional)
     *          'weight_units'    => 'KILOGRAM',
     *          'dimension_units' => 'CENTIMETER' (optional)
     *        )
     *
     * @return array 'Welivery' shipment created data
     *
     * @throws \Magento\Framework\Exception\ValidatorException Errors on the API Response or getting the shipping label
     *
     */
    public function ship(\Magento\Sales\Model\Order\Address $shippingAddress, $package)
    {
        /**
         *
         * Debug API call
         *
         */
        $this->_debug('delivery_create:');
        $this->_debug('shipping_address->');
        $this->_debug($shippingAddress->getData());
        $this->_debug('package->');
        $this->_debug($package);

        $data = $this->_getApi()->createShipment($this->_getDataToShip($shippingAddress, $package));

        /**
         *
         * Validate if API response has errors
         *
         */
        $this->_validateApiResponse($data);

        $shippingLabel = $this->_getShippingLabelContent($data['data']['LabelUrl']);

        /**
         *
         * Validate if there is a failure getting the label content
         *
         */
        if(!$shippingLabel) {
            $this->_debug('Error getting the shipping label');
            throw new \Magento\Framework\Exception\ValidatorException(__('Error getting the shipping label'));
        }
        $data['data']['shipping_label'] = $shippingLabel;

        /**
         *
         * Send staff notifier email
         *
         */
        $this->_sendShippingLabelNotifierEmail($shippingAddress->getOrder()->getIncrementId());

        return $data['data'];
    }

    /**
     *
     * Get 'Welivery' shipment data
     *
     * @param int $customWeliveryId Custom ID used when we created the "Welivery" shipment
     *
     * @return array 'Welivery' shipment data
     *
     */
    public function getData($customWeliveryId)
    {
        /**
         *
         * Debug API call
         *
         */
        $this->_debug('delivery_get:');
        $this->_debug('custom_welivery_id->');
        $this->_debug($customWeliveryId);

        $data = $this->_getApi()->getShipment($customWeliveryId);

        /**
         *
         * Validate if API response has errors
         *
         */
        $this->_validateApiResponse($data);

        return $data['data'];
    }

    /**
     *
     * Cancel 'Welivery'
     *
     * @param int $customWeliveryId Custom ID used when we created the "Welivery" shipment
     *
     * @return array 'Welivery' shipment data
     *
     */
    public function cancel($customWeliveryId)
    {
        /**
         *
         * Debug API call
         *
         */
        $this->_debug('delivery_cancel:');
        $this->_debug('custom_welivery_id->');
        $this->_debug($customWeliveryId);

        $data = $this->_getApi()->cancelShipment($customWeliveryId);

        /**
         *
         * Validate if API response has errors
         *
         */
        $this->_validateApiResponse($data);

        return $data['data'];
    }

    /**
     *
     * Return if automatic guides generation is active
     *
     * @return bool TRUE if it is active | FALSE if it is not active
     *
     */
    public function isShippingLabelGenerationAutomatic()
    {
        return $this->_getConfigData(self::STANDARD_GROUP_ID, 'auto_shipping_label_creation');
    }

    /**
     *
     * Return if the shipping labels generation mode is one per sale
     *
     * @return bool TRUE if the generation mode is one per sale | FALSE if the generation mode is one per item
     *
     */
    public function isOneShippingLabelPerSale()
    {
        return $this->_getConfigData(self::STANDARD_GROUP_ID, 'shipping_labels_mode') == \Ids\Welivery\Model\Config\Source\Shippinglabelsmode::SALE_MODE;
    }

    /**
     *
     * Return API Class to interact with 'Welivery' system
     *
     * @return \Ids\Welivery\Api Class to interact with 'Welivery' API
     *
     */
    protected function _getApi()
    {
        return $this->_apiFactory->create(['url' => $this->_getEntryPoint(), 'apiKey' => $this->_getApiKey(), 'apiSecret' => $this->_getApiSecret()]);
    }

    /**
     *
     * Get shipping label content
     *
     * @param string $labelUrl shipping label URL to get its content
     *
     * @return bool|string The shipping label PDF content or FALSE on failure
     *
     */
    protected function _getShippingLabelContent($labelUrl)
    {
        return file_get_contents($labelUrl);
    }

    /**
     *
     * Validate if the response returned from 'Welivery' API has errors
     *
     * @param array $data Response returned from API call
     *
     * @return void
     *
     * @throws \Magento\Framework\Exception\ValidatorException Errors on the API Response
     *
     */
    protected function _validateApiResponse($data)
    {
        /**
         *
         * Debug API response
         *
         */
        $this->_debug('response:');
        $this->_debug($data);

        /**
         *
         * Validate if there is a failure in the response
         *
         */
        if($data['status'] == self::STATUS_ERROR_CODE) {
            throw new \Magento\Framework\Exception\ValidatorException(__($data['message']));
        }
    }

    /**
     *
     * Get Welivery API config data
     *
     * @param string $groupId system config group ID
     * @param string $fieldId system config field ID
     *
     * @return string
     *
     */
    protected function _getConfigData($groupId, $fieldId)
    {
        return $this->_scopeConfig->getValue(sprintf('shipping/%s/%s', $groupId, $fieldId), \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     *
     * Log debug data to file if debug flag is enable
     *
     * @param mixed $debugData
     *
     * @return void
     *
     */
    protected function _debug($debugData)
    {
        if($this->_getDebugFlag()) {
            $this->_logger->debug(var_export($debugData, true));
        }
    }

    /**
     *
     * Return if the shipping method is in developer mode
     *
     * @return bool
     *
     */
    private function _isDeveloperMode()
    {
        return $this->_getConfigData(self::STANDARD_GROUP_ID, 'mode') == \Ids\Welivery\Model\Config\Source\Mode::DEV_MODE;
    }

    /**
     *
     * Return the group ID in system config to the respective active mode, to get its data
     *
     * @return string
     *
     */
    private function _getConfigGroup()
    {
        if($this->_isDeveloperMode()) {
            return self::DEV_GROUP_ID;
        }

        return self::PROD_GROUP_ID;
    }

    /**
     *
     * Get API entry point
     *
     * @return string
     *
     */
    private function _getEntryPoint()
    {
        return $this->_getConfigData($this->_getConfigGroup(), 'entry_point');
    }

    /**
     *
     * Get API key
     *
     * @return string
     *
     */
    private function _getApiKey()
    {
        return $this->_getConfigData($this->_getConfigGroup(), 'api_key');
    }

    /**
     *
     * Get API secret
     *
     * @return string
     *
     */
    private function _getApiSecret()
    {
        return $this->_getConfigData($this->_getConfigGroup(), 'api_secret');
    }

    /**
     *
     * Get data to create a 'Welivery' shipment
     *
     * @param \Magento\Sales\Model\Order\Address $shippingAddress
     * @param array $package Package to ship. Array example:
     *        array (
     *          'weight'          => '1',
     *          'length'          => '1', (optional)
     *          'width'           => '1', (optional)
     *          'height'          => '1', (optional)
     *          'weight_units'    => 'KILOGRAM',
     *          'dimension_units' => 'CENTIMETER' (optional)
     *        )
     *
     * @return array
     *
     */
    private function _getDataToShip(\Magento\Sales\Model\Order\Address $shippingAddress, $package)
    {
        $customWeliveryId = $this->_generateCustomWeliveryId();
        $shippingData     = $this->_getShippingData($shippingAddress);
        $originData       = $this->_getOriginData();
        $packagesData     = $this->_getPackagesData($package);

        /**
         *
         * Get 'Welivery' shipping and origin data to crete shipment
         *
         */
        $data =  array_merge($shippingData, $originData);

        /**
         *
         * Set custom 'Welivery' Id
         *
         */
        $data['Id'] = $customWeliveryId;

        /**
         *
         * Set firstname lastname
         *
         */
        $data['NameLast'] = $shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname();

        /**
         *
         * Set observations message
         *
         */
        $data['Observations'] = $this->_getObservationsMessage();

        /**
         *
         * Send order increment id
         *
         */
        $data['Complement'] = $shippingAddress->getOrder()->getIncrementId();

        /**
         *
         * Set shipment weight
         *
         */
        $data['WeightG'] = $packagesData['WeightG'];

        /**
         *
         * Set shipment volume if it is set
         *
         */
        if(isset($packagesData['VolumeCm3'])) {
            $data['VolumeCm3'] = $packagesData['VolumeCm3'];
        }

        return $data;
    }

    /**
     *
     * Return a custom ID to create the delivery in 'Welivery' API
     *
     * @return string Return the custom ID. It is the date when the shipment is going to be created
     *
     */
    private function _generateCustomWeliveryId()
    {
        return $this->_date->gmtDate('Ymd-His');
    }

    /**
     *
     * System observations message to set when the 'Welivery' shipment is created
     *
     * @return string Return the custom ID. It is the date when the shipment is going to be created
     *
     */
    private function _getObservationsMessage()
    {
        return $this->_getConfigData(self::STANDARD_GROUP_ID, 'shipping_label_observations');
    }

    /**
     *
     * Return shipping data needed to create 'Welivery' shipment
     *
     * @param \Magento\Sales\Model\Order\Address $shippingAddress
     *
     * @return array
     *
     */
    private function _getShippingData(\Magento\Sales\Model\Order\Address $shippingAddress)
    {
        $data = [];

        foreach($this->_mapShippingData as $key => $value) {
            $data[$key] = $shippingAddress->getData($value);
        }

        return $data;
    }

    /**
     *
     * Return store origin data needed to create 'Welivery' shipment
     *
     * @return array Origin address data to create 'Welivery' shipment
     *
     */
    private function _getOriginData()
    {
        $data = [];

        foreach($this->_mapOriginData as  $key => $value) {
            $data[$key] =  $this->_scopeConfig->getValue($value, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        }

        return $data;
    }

    /**
     *
     * Get 'Welivery' package data needed to create a shipment
     *
     * @param array $package Package to ship. Array example:
     *        array (
     *          'weight'          => '1',
     *          'length'          => '1', (optional)
     *          'width'           => '1', (optional)
     *          'height'          => '1', (optional)
     *          'weight_units'    => 'KILOGRAM',
     *          'dimension_units' => 'CENTIMETER' (optional)
     *        )
     *
     * @return array Return ['weightG' => value, 'VolumeCm3' => value (optional)]
     *
     * @throws \Magento\Framework\Exception\ValidatorException Errors with volume and height units
     *
     */
    private function _getPackagesData($package)
    {
        $packageData = [];

        /**
         *
         * Calculate weight
         *
         */
        switch ($package['weight_units']) {
            case \Zend_Measure_Weight::KILOGRAM:
                $packageData['WeightG'] = $this->_helper->convertKgToG($package['weight']);
                break;

            case \Zend_Measure_Weight::POUND:
                $packageData['WeightG'] = $this->_helper->convertPoundToG($package['weight']);
                break;

            default:
                throw new \Magento\Framework\Exception\ValidatorException(__('Wrong weight unit'));
        }

        /**
         *
         * Calculate volume
         *
         * @note It is not mandatory
         *
         */
        if(isset($package['dimension_units'])) {
            switch ($package['dimension_units']) {
                case \Zend_Measure_Length::CENTIMETER:
                    $packageData['VolumeCm3'] = $package['length'] * $package['width'] * $package['height'];
                    break;

                case \Zend_Measure_Length::INCH:
                    $lenght                   = $this->_helper->convertInchToCm($package['length']);
                    $width                    = $this->_helper->convertInchToCm($package['width']);
                    $height                   = $this->_helper->convertInchToCm($package['height']);
                    $packageData['VolumeCm3'] = $lenght * $width * $height;
                    break;

                default:
                    throw new \Magento\Framework\Exception\ValidatorException(__('Wrong volume unit'));
            }
        }

        return $packageData;
    }

    /**
     *
     * Send the shipping label email staff notifier
     *
     * @param int $orderIncrementId
     *
     * @return void
     *
     * @throws \Magento\Framework\Exception\ValidatorException Error sending the email
     *
     */
    private function _sendShippingLabelNotifierEmail($orderIncrementId)
    {
        try {
            $this->_inlineTranslation->suspend();

            $transport = $this->_transportBuilder->setTemplateIdentifier($this->_getShipTemplateEmail())
                                                 ->setTemplateOptions([
                                                    'area'  => \Magento\Framework\App\Area::AREA_ADMINHTML,
                                                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                                                 ])
                                                 ->setTemplateVars([
                                                    'message'  => $orderIncrementId
                                                 ])
                                                ->setFrom('support')
                                                ->addTo($this->_getShipStaffEmail())
                                                ->getTransport();
            $transport->sendMessage();

            $this->_inlineTranslation->resume();
        }
        catch(\Exception $e) {
            throw new \Magento\Framework\Exception\ValidatorException(__($e->getMessage()));
        }
    }

    /**
     *
     * System template email to notify when a 'Welivery' shipment is created
     *
     * @return string
     *
     */
    private function _getShipTemplateEmail()
    {
        return $this->_getConfigData(self::STANDARD_GROUP_ID, 'email_shipping_label_template');
    }

    /**
     *
     * System staff email to notify when a 'Welivery' shipment is created
     *
     * @return string
     *
     */
    private function _getShipStaffEmail()
    {
        $emailContactId = $this->_getConfigData(self::STANDARD_GROUP_ID, 'staff_shipping_label_notified');
        return $this->_scopeConfig->getValue("trans_email/ident_{$emailContactId}/email", \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     *
     * Return if debug is enabled
     *
     * @return bool
     *
     */
    private function _getDebugFlag()
    {
        return $this->_scopeConfig->getValue('carriers/welivery/debug', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
