<?php
/**
 *
 * @name Ids\Welivery\Model\ResourceModel\Shippingrate
 *
 * @description Resource model to work with 'ids_welivery_shippingrate' table
 *
 * @todo See to use a Repository instead of this CRUD model structure with API Interface
 *
 */
namespace Ids\Welivery\Model\ResourceModel;
class Shippingrate extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     * Initialize resource model
     *
     * @return void
     *
     */
    protected function _construct()
    {
        $this->_init('ids_welivery_shippingrate', 'entity_id');
    }
}